const Router = require('express');
const router = new Router();
const authController = require('../controllers/AuthController');
const {check} = require('express-validator');
const authMiddleware = require('../middleware/authMiddleware');

router.post('/auth/register', [
    check('username', "Username can not be empty").notEmpty(),
    check('password', "Password must be greater than 4 and led than 10 symbols").isLength({min: 4, max:10})
], authController.registration);
router.post('/auth/login', authController.login);
router.get('/users/me', authMiddleware, authController.getProfileInfo);
router.patch('/users/me', authMiddleware, authController.changePassword);
router.delete('/users/me', authMiddleware, authController.deleteProfile);

module.exports = router
