const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {validationResult} = require('express-validator');
const secret = process.env.SECRET;

const generateAccessToken = (id, username) => {
    const payload = {
        id,
        username
    }
    return jwt.sign(payload, secret, {expiresIn: "24h"})
}

class authController {
    async registration(req, res) {
        try {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(400).json({message: "Registration error"})
            }
            const {username, password} = req.body;
            const candidate = await User.findOne({username});
            if(candidate) {
                return res.status(400).json({message: "User with this name has already existed"})
            }
            const hashPassword = bcrypt.hashSync(password, 7);
            const user = new User({username, password: hashPassword});
            await user.save();
            return res.status(200).json({message: "Success"})
        } catch(e) {
            console.log(e);
            res.status(500).json({message: "Internal server error"})
        }
    }
    async login(req, res) {
        try {
            const {username, password} = req.body;
            const user = await User.findOne({username})
            if(!user) {
                return res.status(400).json({message: `User ${username} not found`});
            }
            const validPassword = bcrypt.compareSync(password, user.password);
            if(!validPassword) {
                return res.status(400).json({message: "Incorrect password"});
            }
            const token = generateAccessToken(user._id, user.username);
            return res.status(200).json({message: "Success",
                "jwt_token": token})
        } catch(e) {
            res.status(500).json({message: "Internal server error"})
        }
    }


    async getProfileInfo(req, res) {
        try {
            const user = await User.findOne({
                username: req.user.username
            });
            if(!user) {
                res.status(400).json({message: "No user"})
                return;
            }
            res.status(200).json({"user": {
                "_id": user._id,
                "username": user.username,
                "createdDate":  user.createdAt
                }});
        } catch(e) {
            res.status(500).json({message: "Internal server error"})
        }
    }

    async deleteProfile (req, res) {
        try {
            User.findOneAndRemove({_id: req.user.id}, (err) => {
                if(err) {
                    res.status(400).json({message: "Can not delete"});
                    return;
                }
                res.status(200).json({message: "Success"});
            })
        } catch(e) {
            res.status(500).json({message: "Internal server error"})
        }
    }

    async changePassword (req, res) {
        try {
            const oldPassword = req.body.old;
            const newPassword = req.body.new;
            const user = await User.findOne({
                username: req.user.username
            });

            if (!user) {
                res.status(400).json({message: "User not found"});
                return;
            }
            const passwordEqual = await bcrypt.compare(oldPassword, user.password);
            if (passwordEqual === false) {
                res.status(400).json({message: "Incorrect password"});
                return;
            }
            user.password = await bcrypt.hash(newPassword, 7);
            await user.save();

        } catch (e) {
            res.status(500).json({message: "Internal server error"});
            return;
        }
        res.status(200).json({message: "Success"});
    }
}

module.exports = new authController();