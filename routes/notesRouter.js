const Router = require('express');
const router = new Router();
const notesController = require('../controllers/NotesController');
const authMiddleware = require('../middleware/authMiddleware');

router.post('/notes', authMiddleware, notesController.addNote);
router.get('/notes/:id', authMiddleware, notesController.getNoteById);
router.put('/notes/:id', authMiddleware, notesController.updateNoteById);
router.delete('/notes/:id', authMiddleware, notesController.deleteNoteById);
router.patch('/notes/:id', authMiddleware, notesController.checkNote);

module.exports = router